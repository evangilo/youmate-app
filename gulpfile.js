var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyHTML = require('gulp-minify-html');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sh = require('shelljs');
var clean = require('gulp-clean');
var ngAnnotate = require('gulp-ng-annotate');
var angularFilesort = require('gulp-angular-filesort');
var inject = require('gulp-inject');
var templateCache = require('gulp-angular-templatecache');
var addsrc = require('gulp-add-src');
var image = require('gulp-image');
var removeLogs = require('gulp-removelogs');
var karma = require('karma').server;
var autoprefixer = require('gulp-autoprefixer');

var paths = {
  sass: ['!./scss/**/*.scss', './www/index.scss', './www/**/*.scss', '!./www/vendor/**/*.scss'],
  ionic_sass: ['./scss/**/*.scss'],
  js: ['./www/app/**/*.js', '!./www/vendor/**/*.js', '!./www/app/**/*.test.js'],
  build: {
    css: ['./www/build/**/*.css', '!./www/build/**/*.min.css'],
    js: ['./www/build/**/*.js', '!./www/build/**/*-min.js', '!./www/build/**/*.min.js']
  }
};

gulp.task('default', ['clean-build-dir']);

gulp.task('clean-build-dir', function(done) {
  var files = ['./www/build/**/*.js', './www/build/**/*.css']
  return gulp.src(files, { read: false }).pipe(clean());
})

gulp.task('img', function(done) {
  gulp.src('./www/img/*')
    .pipe(image({
      pngquant: true,
      optipng: true,
      zopflipng: true,
      advpng: true,
      jpegRecompress: true,
      jpegoptim: true,
      mozjpeg: true,
      gifsicle: true,
      svgo: true
    }))
    .pipe(gulp.dest('./www/build/img/'))
    .on('end', done);
});

gulp.task('js', function(done) {
  // var TEMPLATE_HEADER = 'angular.module("<%= module %>", []).run(["$templateCache", function($templateCache) {';
  // gulp.src('./www/app/**/*.html')
  // .pipe(minifyHTML({quotes: true}))
  // .pipe(templateCache({templateHeader: TEMPLATE_HEADER}))
  // .pipe(addsrc(paths.js))
  gulp.src(paths.js)
    .pipe(angularFilesort())
    .pipe(ngAnnotate())
    .pipe(concat('youmate.js'))
    // .pipe(removeLogs())
    .pipe(gulp.dest('./www/build/'))
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest('./www/build/'))
    .on('end', done);
});

function minifySCSS(paths, fileOut) {
  return gulp.src(paths)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(autoprefixer())
    .pipe(concat(fileOut))
    .pipe(gulp.dest('./www/build/'))
    .pipe(minifyCss({ keepSpecialComments: 0 }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/build/'));
}

gulp.task('sass', function() {
  minifySCSS(paths.ionic_sass, 'ionic.css')
  minifySCSS(paths.sass, 'youmate.css');
});

gulp.task('inject-index', ['sass', 'js'], function(done) {
  /*var source_js = gulp.src(paths.build.js).pipe(angularFilesort());
  var source_css = gulp.src(paths.build.css);

  return gulp.src('./www/index.html')
    .pipe(inject(source_js, {relative: true}))
    .pipe(gulp.dest('./www'))
    .pipe(inject(source_css, {relative: true}))
    .pipe(gulp.dest('./www'));*/
});

gulp.task('test', function(done) {
  karma.start({
    configFile: __dirname + '/karma.js',
    singleRun: true
  }, function() {
    done();
  });
});

gulp.task('watch', function() {
  var tasks = ['sass', 'js', 'img'];
  gulp.watch(paths.sass, tasks);
  gulp.watch(paths.js, tasks);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
