(function() {
  'use strict';

  angular
    .module('app', [
      /* ionic */
      'ionic',
      /* modules */
      'app.menus',
      'app.database',
      'app.push',
      'app.api',
      'app.auth',
      'app.after-login',
      'app.profile',
      'app.mates',
      'app.polls',
      'app.chat',
      /* components */
      'components.you-mate-card-list',
      'components.you-interest-list',
      'components.you-interests-edit',
      'components.you-scroll-shrink',
      'components.you-take-photo',
      'components.you-goto-profile',
      'components.you-elastic-textarea',
      'components.you-scroll-to',
      /* blocks */
      'blocks.geolocation',
      'blocks.alerts',
      'blocks.uuid',
      /* third party */
      'ngCordova',
      'ngStorage',
      'angular-input-stars',
      'google.places',
      'ngMask',
      'angular.filter',
      'ngAutocomplete',
    ]);
})();
