(function() {
  'use strict';

  angular
    .module('app.push')
    .service('PushService', PushService);

  /* @ngInject */
  function PushService($rootScope, $location, $state,
                       AuthUserService, YouMatePushType, ChatsDao, MateNotificationsDao) {
    var self = this;
    self.handler = handler;

    function handler(notification) {
      var data = notification.additionalData;
      var redirect = data.coldstart || !data.foreground;
      var user = AuthUserService.getUser();
      if (user == null) return;

      if (data.type == YouMatePushType.CHAT_MESSAGE) {
        data.message = notification.message;
        _handlerMessage(data, user, redirect);
      } else if (data.type == YouMatePushType.INVITE_MATE) {
        _startPendingMatesView(redirect);
      } else if (data.type == YouMatePushType.ACCEPT_MATE) {
        _startProfileView(data, redirect);
      } else if (data.type == YouMatePushType.ANSWER_POLL) {
        _startAnswerPoll(data, redirect);
      }
    }

    function _handlerMessage(data, user, redirect) {
      if (redirect) {
        var id = user.id != data.sent_by ? data.sent_by : data.sent_to;
        $state.go('menu.chat-detail', { target: id });
      }
      ChatsDao.addNotification(data);
      $rootScope.$emit('newMessages', data);
    }

    function _startPendingMatesView(redirect) {
      if (redirect) {
        $state.go('tabs.mates.pending-mates');
      }
      MateNotificationsDao.add();
      $rootScope.$emit('changeNotificationOfMates');
    }

    function _startProfileView(data, redirect) {
      if (redirect) {
        $state.go('menu.profile', { id: data.mateId });
      }
    }

    function _startAnswerPoll(data, redirect) {
      if (redirect) {
        $state.go('menu.answers', {pollId: data.pollId });
      }
    }
  }

})();
