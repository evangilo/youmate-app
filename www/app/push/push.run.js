(function() {
  'use strict';

  angular
    .module('app.push')
    .run(pushRun);

  /* @ngInject */
  function pushRun($rootScope, $ionicPlatform, AppConst, PushService, pubnub) {
    $ionicPlatform.ready(function() {
      var push = PushNotification.init({
        android: {
          senderID: AppConst.GOOGLE_SENDER_ID,
          clearNotifications: false,
          vibrate: true,
          iconColor: '#dd4b38',
          icon: 'ic_stat_youmate'
          // forceShow: true
        }
      });

      push.on('registration', function(data) {
        $rootScope.deviceRegId = data.registrationId;
        console.log('DEVICE_ID', $rootScope.deviceRegId);
        setTimeout(function() {
          pubnub.init();
        }, 5000);
      });

      push.on('notification', function(notification) {
        console.log('notification', notification);
        PushService.handler(notification);
      });

      push.on('error', function(e) {
        alert('Push Error');
      });
    });
  }

})();
