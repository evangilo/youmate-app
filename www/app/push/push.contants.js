(function () {
  'use strict';

  angular
    .module('app.push')
    .constant('YouMatePushType', {
      CHAT_MESSAGE: 'message',
      INVITE_MATE: 'invite_mate',
      ACCEPT_MATE: 'accept_mate',
      ANSWER_POLL: 'answer_poll'
    });

})();
