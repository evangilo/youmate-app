(function() {
  'use strict';

  angular
    .module('app.mates')
    .controller('MyMatesController', MyMatesController);

  function MyMatesController($scope, ApiService) {
    var vm = this;
    vm.loading = true;
    vm.users = [];
    vm.queryMyMates = queryMyMates;

    $scope.$on('$ionicView.beforeEnter', initialize);

    function initialize() {
      vm.loading = true;
      queryMyMates();
    }

    function queryMyMates() {
      ApiService.profiles().my_mates(function(users) {
        vm.loading = false;
        vm.users = users.mates;
        vm.emptyList = vm.users.length == 0;
      }, function(error) {
        vm.loading = false;
      });
    }
  }

})();
