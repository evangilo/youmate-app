(function() {
  'use strict';

  angular
    .module('app.mates')
    .controller('MateListController', MatesController);

  /* @ngInject */
  function MatesController($rootScope, $scope, ApiService, AuthUserService, geolocation, Alerts) {
    var vm = this;
    vm.users = [];
    vm.place = {};
    vm.page = null;
    vm.queryMates = queryMates;
    vm.reloadMates = reloadMates;
    vm.isLogged = AuthUserService.isLogged;

    $scope.$on('$ionicView.loaded', initialize);

    function initialize() {
      vm.page = ApiService.usersPage();
      geolocation.location(function(place) {
        console.log("place", place);
        vm.place = place;
        queryMates();
      });
    }

    function reloadMates() {
      ApiService.usersPage().next(params(), function(users, paginate) {
        $scope.$broadcast('scroll.refreshComplete');
        if (vm.page.page.count != paginate.page.count) {
          vm.page = paginate;
        } else {
          Alerts.showBottomToast('Feed atualizado');
        }
      }, function(error) {
        $scope.$broadcast('scroll.refreshComplete');
      });
    }

    function queryMates() {
      vm.page.next(params(), function(users) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }, function(error) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    }

    function params() {
      var user = AuthUserService.getUser();
      var interests = user ? user.profile.interests.toString() : null;
      var fields = {
        latitude: vm.place.latitude,
        longitude: vm.place.longitude,
        profile__interests__id: interests
      };
      return fields;
    }

    $rootScope.$on('successLogin', function() {
      vm.page.reset();
      queryMates();
    });

    $rootScope.$on('ratingUser', function(event, userId) {
      var index = _.findIndex(vm.page.list, {'id': userId});
      if (index != -1) {
        ApiService.users().get({id: userId}, function(user) {
          vm.page.list[index] = user;
          vm.page.list = _.orderBy(vm.page.list, ['profile.reference_rate', 'profile.references.length'], ['desc', 'desc']);
        });
      }
    });
  }
})();