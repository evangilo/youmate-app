(function() {
  'use strict';

  angular
    .module('app.mates')
    .controller('PendingMatesController', PendingMatesController);

  /* @ngInject */
  function PendingMatesController($rootScope, $scope, ApiService, Alerts, MateNotificationsDao) {
    var vm = this;
    vm.loading = true;
    vm.acceptMate = acceptMate;
    vm.rejectMate = rejectMate;
    vm.queryPendingMates = queryPendingMates;

    $scope.$on('$ionicView.afterEnter', initialize);

    function initialize() {
      queryPendingMates();
    }

    function queryPendingMates() {
      vm.loading = true;
      vm.users = ApiService.profiles().pending_mates(function(users) {
        vm.loading = false;
        vm.users = users.mates;
        vm.emptyList = vm.users.length == 0;
        deletePendingMateNotifications();
      }, function(error) {
        vm.loading = false;
      });
    }

    function acceptMate(id) {
      ApiService.profiles().accept_mate({
        id: id
      }, function(res) {
        _.remove(vm.users, function(user) {
          return user.profile.id == id;
        });
        Alerts.showCenterToast("Mate confirmado");
      });
    }

    function rejectMate(id) {
      ApiService.profiles().reject_mate({
        id: id
      }, function(res) {
        _.remove(vm.users, function(user) {
          return user.profile.id == id;
        });
        Alerts.showCenterToast("Mate recusado.");
      });
    }

    function deletePendingMateNotifications() {
      MateNotificationsDao.deleteAll();
      $rootScope.$emit('changeNotificationOfMates');
    }
  }

})();
