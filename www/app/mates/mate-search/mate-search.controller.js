(function() {
  'use strict';

  angular
    .module('app.mates')
    .controller('MateSearchController', MateSearchController);

  /* @ngInject */
  function MateSearchController($scope, $interval, $state, geolocation, SearchMatesDao) {
    var vm = this;
    vm.place = '';
    vm.location = '';
    vm.searchs = [];
    vm.searchMates = searchMates;
    vm.removeSearch = removeSearch;
    vm.findByPlace = findByPlace;
    vm.findByLocation = findByLocation;

    $scope.$on('$ionicView.afterEnter', function() {
      querySearchs();
      setUpLocation();
    });

    function setUpLocation() {
      geolocation.location(function(place) {
        vm.location = place.city;
      });
    }

    function findByPlace() {
      searchMates(vm.place);
    }

    function findByLocation() {
      searchMates(vm.location);
    }

    function searchMates(place) {
      console.log('place', place);
      SearchMatesDao.add(place);
      $state.go('menu.mate-result', { 'place': place });
    }

    function querySearchs() {
      SearchMatesDao.all().then(function(searchs) {
        vm.searchs = searchs;
      });
    }

    function removeSearch(index, search) {
      vm.searchs.splice(index, 1);
      SearchMatesDao.remove(search.id);
    }
  }
})();
