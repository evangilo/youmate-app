(function() {
  'use strict';

  angular
    .module('app.mates')
    .controller('MateResultController', MateResultController);

  /* @ngInject */
  function MateResultController($scope, $stateParams, $ionicModal, ApiService) {
    var vm = this;
    vm.page = null;
    vm.filters = {
      username: "",
      interests: null,
      status: null,
      genre: null
    };
    vm.queryMates = queryMates;
    vm.applyFilters = applyFilters;
    vm.showFilterModal = showFilterModal;
    vm.hideFilterModal = hideFilterModal;

    $scope.$on('$ionicView.afterEnter', initialize);

    function initialize() {
      resetPage();
      setUpFilterModal();
      queryMates();
    }

    function resetPage() {
      vm.page = ApiService.usersPage();
    }

    function queryMates() {
      vm.page.next(getParams(), function(users) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }, function(error) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });
    }

    function applyFilters() {
      resetPage();
      hideFilterModal();
    }

    function getParams() {
      var name = vm.filters.username.split(" ");
      return {
        address: $stateParams.place,
        first_name__icontains: name[0],
        last_name__icontains: name[1],
        profile__interests__id: vm.filters.interests,
        profile__genre__icontains: vm.filters.genre,
        profile__status: vm.filters.status
      };
    }

    function setUpFilterModal() {
      $ionicModal.fromTemplateUrl('app/mates/mate-result/filter-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modal = modal;
      });
    }

    function showFilterModal() {
      $scope.modal.show();
    }

    function hideFilterModal() {
      $scope.modal.hide();
    }
  }
})();
