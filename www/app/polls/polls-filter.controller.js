(function () {
  'use strict';

  angular
    .module('app.polls')
    .controller('PollsFilterController', PollsFilterController);

  /* @ngInject */
  function PollsFilterController($rootScope, $ionicHistory, $localStorage) {
    var vm = this;
    vm.interests = getInterests();

    vm.apply = function() {
      saveInterests(vm.interests);
      $rootScope.$emit('reloadPolls');
      $ionicHistory.goBack();
    }

    function getInterests() {
      return angular.copy($localStorage.interests) || [];
    }

    function saveInterests(interests) {
      $localStorage.interests = angular.copy(interests);
    }
  }
})();