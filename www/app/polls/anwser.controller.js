(function () {
  'use strict';

  angular
    .module('app.polls')
    .controller('AnwserController', AnwserController);

  /* @ngInject */
  function AnwserController($scope, $state, $ionicModal, PollsService, AuthUserService) {
    var vm = this;
    vm.pollId = null;
    vm.poll = {};
    $scope.newAnwser = {
      pollId: null,
      text: ''
    };

    vm.likeAction = likeAction;
    vm.openAnswerCreateModal = openAnswerCreateModal;
    $scope.saveNewAnwser = saveNewAnwser;

    $scope.$on('$ionicView.afterEnter', function() {
      vm.pollId = $state.params.pollId;
      $scope.newAnwser.pollId = vm.pollId;
      vm.poll = PollsService.get(vm.pollId);
      setupAnwserCreateModal();
    });


    function likeAction(index) {
      var answer = vm.poll.answers[index];
      PollsService
        .likeAction(vm.pollId, answer.id, answer.liked)
        .then(function(response) {
          answer.liked = !answer.liked;
          if (answer.liked) {
            answer.likes += 1;
          } else {
            answer.likes -= 1;
          }
        });
    }

    function saveNewAnwser() {
      console.log('saveNewAnwser');
      PollsService.createAnwser($scope.newAnwser).then(function(answer) {
        var user = AuthUserService.getUser();
        answer.user = {
          id: user.id,
          full_name: user.full_name,
          picture: user.profile.photo_url,
          living_city: user.profile.living_city,
        };
        answer.likes =  0;
        answer.liked = false;
        vm.poll.answers.push(answer);
      })
      $scope.anwserModal.hide();
    }

    function openAnswerCreateModal() {
      $scope.anwserModal.show().then(function() {
        $scope.newAnwser.text = '';
      });
    }

    function setupAnwserCreateModal() {
      $ionicModal
        .fromTemplateUrl('app/polls/anwser-create.modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        })
        .then(function (modal) {
          $scope.anwserModal = modal;
        });
    }
  }
})();
