(function () {
  'use strict';

  angular
    .module('app.polls')
    .controller('PollsCreateController', PollsCreateController);

  /* @ngInject */
  function PollsCreateController($rootScope, $scope, $ionicHistory, PollsService, Alerts) {
    var vm = this;
    vm.saveNewPoll = saveNewPoll;
    vm.isValid= false;

    resetData();

    function resetData() {
      vm.interests = [];
      vm.text = '';
      vm.city = '';
      vm.location = {
        geometry: {}
      };
    }

    function saveNewPoll() {
      Alerts.showProgressDialog('', 'Criando pergunta...');
      PollsService.create({
        text: vm.text,
        address: vm.city,
        interests: vm.interests,
        latitude: vm.location.geometry.latitude,
        longitude: vm.location.geometry.longitude
      }).then(function(poll) {
        $rootScope.$emit('reloadPolls');
        Alerts.hideProgressDialog();
        Alerts.showCenterToast('Pergunta criada com sucesso!');
        $ionicHistory.goBack();
        resetData();
        $rootScope.$emit('resetInterestsSelected');
      }).catch(function(error) {
        Alerts.hideProgressDialog();
        Alerts.showCenterToast('Desculpe, tivemos um problema, tente mais tarde :(');
      });
    }

    function valid() {
      vm.isValid = vm.text !== '' && vm.city !== '' && vm.interests.length > 0;
    }

    $scope.$watchCollection('polls.interests', valid);
    $scope.$watch('polls.text', valid);
    $scope.$watch('polls.city', valid);
  }
})();
