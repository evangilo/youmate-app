(function () {
  'use strict';

  angular
    .module('app.polls')
    .controller('PollsController', PollsController);

  /* @ngInject */
  function PollsController($rootScope, $scope, $ionicModal, $state, $timeout,
                           $localStorage, geolocation, PollsService, Alerts) {
    var vm = this;
    vm.list = [];
    vm.loaded = false;
    vm.disableButton = false;
    $scope.newAnwser = {
      pollId: null,
      text: ''
    };
    $scope.location = {city: '', geometry: {}};

    vm.findPolls = findPolls;
    vm.selectTextInputCity = selectTextInputCity;
    $scope.saveNewAnwser = saveNewAnwser;
    vm.openPollCreateModal = openPollCreateModal;
    vm.openPollsFilter = openPollsFilter;
    vm.openAnwserCreateModal = openAnwserCreateModal;
    vm.inputFocus = inputFocus;
    vm.inputBlur = inputBlur;
    vm.emptyMsg = null;

    $scope.$on('$ionicView.beforeEnter', function() {
      setupLocation();
      setupAnwserCreateModal();
    });

    function findPolls() {
      if (!$scope.location.city ||$scope.location.city === '') {
        vm.emptyMsg = 'Entre com o nome de uma cidade.';
        return;
      }
      $rootScope.lastLocation = angular.copy($scope.location);
      vm.list = [];
      var args = {
        interests__id: $localStorage.interests || [],
        city: $scope.location.city,
        latitude: $scope.location.geometry.latitude,
        longitude: $scope.location.geometry.longitude
      };
      vm.loaded = false;
      handleEmptyMsg();
      PollsService.list(args).then(function(polls) {
        vm.list = polls;
        vm.loaded = true;
        handleEmptyMsg();
      }).catch(function() {
        vm.loaded = true;
        handleEmptyMsg();
      })
    }

    function handleEmptyMsg() {
      if (!vm.loaded) {
        vm.emptyMsg = 'Buscando perguntas...';
      } else if (vm.list.length == 0) {
        if (!$localStorage.interests || $localStorage.interests.length == 0) {
          vm.emptyMsg = 'Não foi encontrada nenhuma pergunta.';
        } else {
          vm.emptyMsg = 'Não foi encontrada nenhuma pergunta em relação aos ' +
                        'interesses escolhidos';
        }
      } else {
        vm.emptyMsg = null;
      }
    }

    function selectTextInputCity(event) {
      event.target.select();
    }

    function saveNewAnwser() {
      PollsService.createAnwser($scope.newAnwser);
      $scope.anwserModal.hide();
      Alerts.showCenterToast('Resposta enviada com sucesso!');
    }

    function openAnwserCreateModal(pollId) {
      $scope.anwserModal.show().then(function() {
        $scope.newAnwser.pollId = pollId;
        $scope.newAnwser.text = '';
      })
    }

    function openPollsFilter() {
      $state.go('menu.pollsFilter');
    }

    function openPollCreateModal() {
      $state.go('menu.polls-create')
    }

    function setupLocation() {
      if ($rootScope.lastLocation) {
        $scope.location = $rootScope.lastLocation;
        findPolls();
      } else {
        Alerts.showProgressDialog('', 'Recuperando localização');
        geolocation.location(function(location) {
          $scope.location.city = location.city;
          $scope.location.geometry.latitude = location.latitude;
          $scope.location.geometry.longitude = location.longitude;
          Alerts.hideProgressDialog();
          if (!location.city || location.city === '') {
            vm.emptyMsg = 'Não foi possível recuperar a sua localização, ' +
                          'entre com o nome de uma cidade no campo de texto.';
          } else {
            findPolls();
          }
        });
      }
    }

    function setupAnwserCreateModal() {
      $ionicModal
        .fromTemplateUrl('app/polls/anwser-create.modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        })
        .then(function (modal) {
          $scope.anwserModal = modal;
        });
    }

    function inputFocus() {
      vm.disableButton = true;
    }

    function inputBlur() {
      $timeout(function () {
        vm.disableButton = false;
      }, 300);
    }

    $rootScope.$on('reloadPolls', findPolls);

    $scope.$watch('location', function(newValue, oldValue) {
      if (newValue.city != oldValue.city &&
          newValue.geometry.latitude == oldValue.geometry.latitude &&
          newValue.geometry.longitude == oldValue.geometry.longitude) {
        $scope.location.geometry = {};
      }
    }, true);
  }
})();
