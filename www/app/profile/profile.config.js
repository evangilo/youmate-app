(function() {
  'use strict';

  angular
    .module('app.profile')
    .run(runProfileConfig);

  /* @ngInject */
  function runProfileConfig(ProfileService) {
    ProfileService.updateStatusLocation();
  }
})();
