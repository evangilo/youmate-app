(function () {
  'use strict';

  angular
    .module('app.profile')
    .service('ProfileService', ProfileService);

  /* @ngInject */
  function ProfileService($q, ApiService, Alerts, AuthUserService, geolocation, pubnub) {
    var self = this;
    self.loadUser = loadUser;
    self.loadUserLogged = loadUserLogged;
    self.updateUser = updateUser;
    self.loadReferences = loadReferences;
    self.sendRating = sendRating;
    self.calculatePercentageProfile = calculatePercentageProfile;
    self.addMate = addMate;
    self.undoMate = undoMate;
    self.cancelInviteMate = cancelInviteMate;
    self.updateStatusLocation = updateStatusLocation;

    function loadUserLogged() {
      return queryUser(ApiService.me, null);
    }

    function loadUser(id) {
      return queryUser(ApiService.users, id);
    }

    function loadReferences(profileId, callback) {
      return ApiService.references().query({
        to_user: profileId
      }, function (references) {
        if (callback) callback(references);
      });
    }

    function sendRating(rating) {
      var promise = $q.defer();

      function _success(reference) {
        hideSpinnerDialog();
        promise.resolve(reference);
      }

      function _error(error) {
        hideSpinnerAndShowMessageError();
        promise.reject(error);
      }
      showSpinnerDialog("Enviando Avaliação...");

      if (rating['id']) {
        ApiService.references().update({
          id: rating.id
        }, rating, _success, _error);
      } else {
        ApiService.references().save(rating, _success, _error);
      }

      return promise.promise;
    }

    function queryUser(userApi, id) {
      var promise = $q.defer();
      showSpinnerDialog("Carregando...");
      userApi().get({
        id: id
      }, function (user) {
        hideSpinnerDialog();
        promise.resolve(user);
      }, function (error) {
        hideSpinnerAndShowMessageError();
        promise.reject(error);
      });
      return promise.promise;
    }

    function updateUser(user, fields) {
      showSpinnerDialog("Salvando...");

      var promise = $q.defer();
      var userData = {first_name: user.first_name, last_name: user.last_name};
      var dataProfile = {};

      fields.forEach(function (field) {
        dataProfile[field] = user.profile[field];
      });

      ApiService.users().patch({id: user.id}, userData, function(user) {
        ApiService.profiles().patch({id: user.profile.id}, dataProfile, function(profile) {
          user.profile = profile;
          AuthUserService.setUser(user);
          promise.resolve(user);
          hideSpinnerDialog();
        }, function(error) {
          promise.reject(error);
          hideSpinnerAndShowMessageError();
        });
      }, function(error) {
        promise.reject(error);
        hideSpinnerAndShowMessageError();
      });

      return promise.promise;
    }

    function updateStatusLocation() {
      if (!AuthUserService.isLogged()) {
        return;
      }
      ApiService.me().get(function(user) {
        AuthUserService.setUser(user);
        geolocation.distanceBetweenCurrentLocation(user.profile.living_city, function(distance) {
          var isTraveller = distance > 50000 ? 'T' : 'L';
          ApiService.profiles().patch({id: user.profile.id}, {
            status: isTraveller,
            interests: user.profile.interests
          });
        });
      });
    }

    function calculatePercentageProfile(profile) {
      var LOGIN_PERCENTAGE = 25.0;
      var PHONE_PERCENTAGE = 25.0;
      var EMAIL_PERCENTAGE = 20.0;
      var sumPercentage = LOGIN_PERCENTAGE;

      var FIELDS = [
        'languages',
        'interests',
        'photo_url',
        'about',
        'job_title',
        'living_city',
      ];

      var FIELD_PERCENTAGE = 30.0 / FIELDS.length;
      FIELDS.forEach(function (key) {
        sumPercentage += profile[key] ? FIELD_PERCENTAGE : 0;
      });

      if (profile['is_phone_verified']) sumPercentage += PHONE_PERCENTAGE;
      if (profile['is_email_verified']) sumPercentage += EMAIL_PERCENTAGE;

      return Math.ceil(sumPercentage);
    }

    function addMate(profileId) {
      var promise = $q.defer();
      showSpinnerDialog("Enviando convite.");
      ApiService.profiles().add_mate({}, {
        id: profileId
      }, function () {
        hideSpinnerDialog();
        promise.resolve();
      }, function (error) {
        hideSpinnerAndShowMessageError();
        promise.reject();
      });
      return promise.promise;
    }

    function undoMate(profileId) {
      var promise = $q.defer();

      function _undoMate() {
        showSpinnerDialog("Desfazendo Mate.");
        ApiService.profiles().delete_mate({}, {
          id: profileId
        }, function () {
          hideSpinnerDialog();
          promise.resolve();
        }, function (error) {
          hideSpinnerAndShowMessageError();
          promise.reject(error);
        });
      }
      showConfirm("Deseja desfazer Mate?", _undoMate);

      return promise.promise;
    }

    function cancelInviteMate(profileId) {
      var promise = $q.defer();

      function _cancelInviteMate() {
        showSpinnerDialog("Cancelando solicitação...");
        ApiService.profiles().cancel_mate({}, {
          id: profileId
        }, function () {
          hideSpinnerDialog();
          promise.resolve();
        }, function (error) {
          hideSpinnerAndShowMessageError();
          promise.reject(error);
        });
      };
      showConfirm("Deseja cancelar o convite?", _cancelInviteMate);
      return promise.promise;
    }

    function showSpinnerDialog(message) {
      Alerts.showProgressDialog("", message);
    }

    function hideSpinnerDialog() {
      Alerts.hideProgressDialog();
    }

    function hideSpinnerAndShowMessageError(message) {
      hideSpinnerDialog();
      showMessage(message || "Aconteceu um erro, tente mais tarde.");
    }

    function showMessage(message) {
      Alerts.showCenterToast(message);
    }

    function showConfirm(message, okCallback) {
      Alerts.showConfirm({
        title: "",
        message: message,
        okText: "Sim",
        cancelText: "Não",
        okCallback: okCallback
      });
    }
  }
})();
