(function() {
  'use strict';

  angular
    .module('app.profile')
    .controller('ProfileViewController', ProfileViewController);

  /* @ngInject */
  function ProfileViewController($scope, $rootScope, $state, $ionicModal, ProfileService, ProfileConst, AuthUserService) {
    var vm = this;
    vm.user = null;
    vm.me = AuthUserService.getUser();
    vm.references = [];
    vm.profilePercentage = 0;
    vm.isMate = false;
    vm.actionMate = { text: null, action: null };
    vm.rating = { rating: 1, text: null };
    vm.ratingAux = null;

    vm.openChat = openChat;
    vm.sendRating = sendRating;
    vm.openRating = openRating;
    vm.closeRating = closeRating;

    $scope.$on('$ionicView.afterEnter', initialize);

    $scope.$on('$ionicView.beforeLeave', destroy);

    function initialize() {
      loadUser();
      setUpRatingModal();
    }

    function destroy() {
      vm.user = null;
    }

    function loadUser() {
      vm.user = null;
      ProfileService.loadUser($state.params.id).then(function(user) {
        setUser(user);
        setUpActionMate();
        loadReferences(user.profile.id);
        calculatePercentageProfile();
      });
    }

    function loadReferences(profileId) {
      ProfileService.loadReferences(profileId, function(references) {
        var user = AuthUserService.getUser();
        var myIndexReview = _.findIndex(references, {'from_user': user.id});

        if (myIndexReview != -1) {
          vm.rating = references[myIndexReview];
          references.splice(myIndexReview, 1);
        }
        vm.references = references;
      });
    }

    function setUser(user) {
      vm.user = user;
    }

    function calculatePercentageProfile() {
      vm.profilePercentage = ProfileService.calculatePercentageProfile(vm.user.profile);
    }

    function openChat() {
      $state.go('menu.chat-detail', { 'target': JSON.stringify(vm.user) });
    }

    function setUpRatingModal() {
      $ionicModal
        .fromTemplateUrl('app/profile/profile-view/rating.modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        })
        .then(function(modal) {
          $scope.modal = modal;
        });
    }

    function sendRating() {
      vm.ratingAux.to_user = vm.user.id;
      ProfileService.sendRating(vm.ratingAux)
        .then(function(reference) {
          vm.rating = reference;
          closeRating();
          $rootScope.$emit('ratingUser', vm.user.id);
        }, function(error) {
          closeRating();
        });
    }

    function openRating() {
      vm.ratingAux = angular.copy(vm.rating);
      $scope.modal.show();
    }

    function closeRating() {
      $scope.modal.hide();
    }

    function setUpActionMate() {
      var mateStatus = vm.user.profile.mate_status;
      if (mateStatus == ProfileConst.IS_MATE) {
        vm.actionMate.text = "Desfazer Mate";
        vm.actionMate.action = undoMate;
        vm.isMate = true;
      } else if (mateStatus == ProfileConst.NOT_IS_MATE) {
        vm.actionMate.text = "Adicionar Mate";
        vm.actionMate.action = addMate;
        vm.isMate = false;
      } else if (mateStatus == ProfileConst.PENDING_INVITE) {
        vm.actionMate.text = "Cancelar Convite";
        vm.actionMate.action = cancelInviteMate;
        vm.isMate = false;
      }
    }

    function addMate() {
      ProfileService.addMate(vm.user.profile.id).then(function() {
        vm.user.profile.mate_status = ProfileConst.PENDING_INVITE;
        setUpActionMate();
      });
    }

    function undoMate() {
      ProfileService.undoMate(vm.user.profile.id).then(function() {
        vm.user.profile.mate_status = ProfileConst.NOT_IS_MATE;
        setUpActionMate();
      });
    }

    function cancelInviteMate() {
      ProfileService.cancelInviteMate(vm.user.profile.id).then(function() {
        vm.user.profile.mate_status = ProfileConst.NOT_IS_MATE;
        setUpActionMate();
      });
    }
  }
})();
