(function() {
  'use strict';

  angular
    .module('app.profile')
    .controller('PhotosController', PhotosController);

  /* @ngInject */
  function PhotosController($scope, $stateParams, Alerts, ApiService, ProfileService) {
    var vm = this;
    vm.profileId = null;
    vm.photos = [];
    vm.savePhotoProfile = savePhotoProfile;
    vm.addPhoto = addPhoto;
    vm.deletePhoto = deletePhoto;

    $scope.$on('$ionicView.afterEnter', initialize);

    function initialize() {
      ApiService.users().get({id: $stateParams.userId}, function(user) {
        vm.user = user;
        vm.photos = ApiService.photos().query({profile: vm.user.profile.id});
      });
    }

    function savePhotoProfile(photo) {
      vm.user.profile.photo = photo;
      vm.user.profile.photo_url = photo;
      ProfileService.updateUser(vm.user, ['photo', 'interests']);
    }

    function addPhoto(photo) {
      Alerts.showProgressDialog(null, "Salvando imagem...");

      var data = {profile: vm.user.profile.id, image: photo};
      ApiService.photos().save(data, function(res) {
        Alerts.hideProgressDialog();
        vm.photos.push(res);
      }, function(error) {
        Alerts.hideProgressDialog();
        Alerts.showBottomToast("Aconteceu um erro, tente novamente.");
      });
    }

    function deletePhoto(index) {
      var options = {
        message: 'Exclur image?',
        okText: 'Sim',
        cancelText: 'Não',
        okCallback: function() {
          ApiService.photos().delete({}, {
            id: vm.photos[index].id
          });
          vm.photos.splice(index, 1);
        }
      };
      Alerts.showConfirm(options);
    }
  }
})();
