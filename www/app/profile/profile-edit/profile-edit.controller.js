(function () {
  'use strict';

  angular
    .module('app.profile')
    .controller('ProfileEditController', ProfileEditController);

  /* @ngInject */
  function ProfileEditController($scope, $ionicModal, $ionicSlideBoxDelegate,
    ApiService, ProfileService, Alerts, $ionicPopover) {
    var vm = this;
    vm.user = null;
    vm.userAux = null;
    vm.profilePercentage = 0;
    vm.references = [];
    vm.languages = [];

    /* Modal Form */
    vm.fields = [];
    vm.templateFormUrlModal = null;
    vm.BASE_TEMPLATE_URL_MODAL = 'app/profile/profile-edit/modals/';
    /* Motal Methods */
    vm.showOverviewModal = showOverviewModal;
    vm.showAboutModal = showAboutModal;
    vm.showInterestsModal = showInterestsModal;
    vm.showPhoneNumberModal = showPhoneNumberModal;
    vm.showLanguagesModal = showLanguagesModal;
    vm.hideProfileModal = hideProfileModal;
    vm.checkLanguage = checkLanguage;

    vm.updateUser = updateUser;
    vm.showConfirmEmail = showConfirmEmail;
    vm.selectReference = selectReference;
    vm.removeReferenceOfAboutText = removeReferenceOfAboutText;
    vm.openMenu = openMenu;
    vm.savePhotoProfile = savePhotoProfile;
    vm.hideLanguagesModal = hideLanguagesModal;

    $scope.$on('$ionicView.beforeEnter', reset);

    $scope.$on('$ionicView.afterEnter', initialize);

    $scope.$on('$ionicView.beforeLeave', reset);

    function initialize() {
      setupMenuPopover();
      loadUser();
      loadLanguages();
      setUpProfileModal();
    }

    function reset() {
      vm.user = null;
      vm.userAux = null;
    }

    function loadUser() {
      vm.user = null;
      ProfileService.loadUserLogged().then(function (user) {
        setUser(user);
        setReferences(user.profile.id);
        calculatePercentageProfile();
      });
    }

    function loadLanguages() {
      vm.languages = ApiService.languages().query();
    }

    function checkLanguage(id) {
      var language = _.find(vm.languages, {id: id});
      if (language.check)
        vm.userAux.profile.language_ids.push(language.id);
      else
        _.remove(vm.userAux.profile.language_ids, function (id) { return id == language.id; });
    }

    function setReferences(profileId) {
      vm.references = ProfileService.loadReferences(profileId);
    }

    function handlerAddreess() {
      var address = vm.userAux.profile.living_city;
      if (address != null && typeof (address) == 'object' && address['formatted_address']) {
        address = address['formatted_address'];
      }
      vm.userAux.profile.living_city = address;
    }

    function handlerAbout() {
      if (vm.userAux.profile['aboutReference']) {
        vm.userAux.profile.about += vm.userAux.profile.aboutReference;
      }
    }

    function handlerLanguages() {
      if (vm.userAux.profile.language_ids.length == 0) {
        vm.userAux.profile.language_ids = [];
      } else {
        vm.userAux.profile.language_ids = "" + vm.userAux.profile.language_ids;
      }
    }

    function updateUser() {
      handlerLanguages();
      handlerAddreess();
      handlerAbout();

      ProfileService.updateUser(vm.userAux, vm.fields).then(function (user) {
          vm.user = user;
          hideProfileModal();
          hideLanguagesModal();
        }, function (error) {
          hideProfileModal();
          hideLanguagesModal();
        });
    }

    function savePhotoProfile(photo) {
      var fields = ['photo', 'interests'];
      vm.userAux.profile.photo = photo;
      ProfileService
        .updateUser(vm.userAux, fields)
        .then(function (user) {
          vm.user = user;
          $ionicSlideBoxDelegate.update();
        });
    }

    function setUser(user) {
      vm.user = user;
      vm.userAux = angular.copy(user);
    }

    function openMenu(event) {
      $scope.popover.show(event);
    }

    function closeMenu() {
      $scope.popover.hide();
    }

    function showConfirmEmail() {
      Alerts.showConfirm({
        title: 'Email não confirmado',
        message: 'Deseja reenviar um novo e-mail de confirmação? Por favor confira se o e-mail está na sua lixeira.',
        okText: 'Sim',
        cancelText: 'Não',
        okCallback: resendEmail
      });
    }

    function resendEmail() {
      ApiService.resendEmail().save(function (res) {
        Alerts.showCenterToast("Email enviado aguarde alguns instantes e confira na sua caixa de email");
      }, function (err) {
        Alerts.showCenterToast("Falha ao enviar email");
      });
    }

    function selectReference(index) {
      var reference = vm.references[index];
      var fromUser = reference.from_user__first_name + ' ' + reference.from_user__last_name;
      vm.userAux.profile.about = reference.text;
      vm.userAux.profile.aboutReference = '<span>' + fromUser + '</span>';
    }

    function removeReferenceOfAboutText() {
      vm.userAux.profile.aboutReference = '';
    }

    function showOverviewModal() {
      var fields = [
        'first_name',
        'last_name',
        'genre',
        'living_city',
        'job_title',
        'interests'
      ];
      showProfileModal('overview', fields);
    }

    function showAboutModal() {
      var fields = ['about', 'interests'];
      showProfileModal('about', fields);
    }

    function showInterestsModal() {
      var fields = ['interests'];
      showProfileModal('interests', fields);
    }

    function showPhoneNumberModal() {
      var fields = ['phone', 'interests'];
      showProfileModal('phone-number', fields);
    }

    function setUpProfileModal() {
      $ionicModal
        .fromTemplateUrl('app/profile/profile-edit/modals/profile-edit.modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        })
        .then(function (modal) {
          $scope.modal = modal;
        });

      $ionicModal
        .fromTemplateUrl('app/profile/profile-edit/modals/languages.modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        })
        .then(function(modal) {
          $scope.languagesModal = modal;
        })
    }

    function showLanguagesModal() {
      vm.fields = ['language_ids', 'interests'];
      vm.languages = vm.languages.map(function(language) {
        language.check = _.includes(vm.user.profile.language_ids, language.id);
        return language;
      });
      vm.userAux = angular.copy(vm.user);
      $scope.languagesModal.show();
    }

    function hideLanguagesModal() {
      $scope.languagesModal.hide();
    }

    function showProfileModal(template, fields) {
      vm.templateFormUrlModal = vm.BASE_TEMPLATE_URL_MODAL + template + '.modal.html';
      vm.fields = fields;
      vm.userAux = angular.copy(vm.user);
      if (vm.userAux.profile.about) {
        vm.userAux.profile.about = vm.user.profile.about.replace(/<span>.*<\/span>/g, '');
      }
      $scope.modal.show();
    }

    function hideProfileModal() {
      vm.userAux = null;
      $scope.modal.hide();
    }

    function calculatePercentageProfile() {
      vm.profilePercentage = ProfileService.calculatePercentageProfile(vm.user.profile);
    }

    function setupMenuPopover() {
      $ionicPopover.fromTemplateUrl('app/profile/profile-edit/menu.html', {
        scope: $scope
      }).then(function (popover) {
        $scope.popover = popover;
      });
    }
  }
})();
