(function() {
  'use strict';

  angular
    .module('app.profile')
    .constant('ProfileConst', {
      'IS_MATE': 'M',
      'NOT_IS_MATE': null,
      "PENDING_INVITE": 'P'
    })

})();
