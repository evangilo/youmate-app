(function() {
  'use strict';

  angular
    .module('app')
    .config(configApp);

  /* @ngInject */
  function configApp($ionicConfigProvider) {
    $ionicConfigProvider.scrolling.jsScrolling(false);
  }
})();
