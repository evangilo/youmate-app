(function() {
  'use strict';

  angular
    .module('app')
    .run(runApp);

  /* @ngInject */
  function runApp($ionicPlatform, $rootScope) {
    $ionicPlatform.ready(function() {
      FastClick.attach(document.body);
      $rootScope.showSplashScreenSVG = true;
      var isAndroid = ionic.Platform.isAndroid();
      var device = ionic.Platform.device();

      if (isAndroid) {
        var oldVersions = ['2.', '4.0', '4.1', '4.2'];
        for (var i in oldVersions) {
          if (device.version.startsWith(oldVersions[i])) {
            $rootScope.showSplashScreenSVG = false;
            break;
          }
        }
      }
    });
  }
})();
