(function() {
  'use strict';

  angular
    .module('app')
    .config(configRoute);

  /* @ngInject */
  function configRoute($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('tabs', {
        url: '/tabs',
        abstract: true,
        templateUrl: 'app/menus/tabs.html',
        controller: 'TabsController as tabs'
      })
      .state('tabs.mates', {
        url: '/friends',
        nativeTransitions: null,
        views: {
          'tabs-menu-content': {
            templateUrl: 'app/menus/mates.html'
          }
        }
      })
      .state('menu', {
        url: '/menu',
        abstract: true,
        templateUrl: 'app/menus/menu.html'
      })
      .state('search', {
        url: '/search',
        abstract: true,
        templateUrl: 'app/menus/search-menu.html'
      })
      .state('menu.login', {
        url: '/login',
        cache: false,
        views: {
          'menu-content': {
            templateUrl: 'app/auth/auth.login.html',
            controller: 'LoginController as vm'
          }
        }
      })
      .state('menu.after-login', {
        url: '/after-login',
        views: {
          'menu-content': {
            templateUrl: 'app/after-login/after-login.html',
            controller: 'AfterLoginController as vm'
          }
        }
      })
      .state('tabs.mate-list', {
        url: '/mates',
        nativeTransitions: null,
        views: {
          'tabs-menu-content': {
            controller: 'MateListController as vm',
            templateUrl: 'app/mates/mate-list/mate-list.html'
          }
        }
      })
      .state('tabs.polls', {
        url: '/polls',
        nativeTransitions: null,
        views: {
          'tabs-menu-content': {
            controller: 'PollsController as polls',
            templateUrl: 'app/polls/polls.html'
          }
        }
      })
      .state('menu.answers', {
        url: '/answers/:pollId',
        nativeTransitions: null,
        views: {
          'menu-content': {
            controller: 'AnwserController as vm',
            templateUrl: 'app/polls/anwser.html'
          }
        }
      })
      .state('menu.polls-create', {
        url: '/polls-create',
        views: {
          'menu-content': {
            controller: 'PollsCreateController as polls',
            templateUrl: 'app/polls/polls-create.html'
          }
        }
      })
      .state('menu.pollsFilter', {
        url: '/polls-filter',
        views: {
          'menu-content': {
            controller: 'PollsFilterController as polls',
            templateUrl: 'app/polls/polls-filter.html'
          }
        }
      })
      .state('search.mate-search', {
        url: '/mate-search',
        views: {
          'menu-content': {
            controller: 'MateSearchController as vm',
            templateUrl: 'app/mates/mate-search/mate-search.html'
          }
        }
      })
      .state('menu.mate-result', {
        url: '/mate-result/:place',
        views: {
          'menu-content': {
            controller: 'MateResultController as vm',
            templateUrl: 'app/mates/mate-result/mate-result.html'
          }
        }
      })
      .state('tabs.mates.my-mates', {
        url: '/my-mates',
        nativeTransitions: null,
        views: {
          'tabs-mates-content-menu': {
            templateUrl: 'app/mates/my-mates/my-mates.html',
            controller: 'MyMatesController as vm'
          }
        }
      })
      .state('tabs.mates.pending-mates', {
        url: '/pending-mates',
        nativeTransitions: null,
        views: {
          'tabs-mates-content-menu': {
            templateUrl: 'app/mates/pending-mates/pending-mates.html',
            controller: 'PendingMatesController as vm'
          }
        }
      })
      .state('menu.profile', {
        url: '/profile/:id',
        views: {
          'menu-content': {
            controller: 'ProfileViewController as vm',
            templateUrl: 'app/profile/profile-view/profile-view.html'
          }
        }
      })
      .state('menu.profile-edit', {
        url: '/profile/edit/',
        views: {
          'menu-content': {
            controller: 'ProfileEditController as vm',
            templateUrl: 'app/profile/profile-edit/profile-edit.html'
          }
        }
      })
      .state('menu.photos', {
        url: '/photos/:userId',
        views: {
          'menu-content': {
            controller: 'PhotosController as vm',
            templateUrl: 'app/profile/photos/photos.html'
          }
        }
      })
      .state('tabs.chat-list', {
        url: '/chat-list',
        nativeTransitions: null,
        views: {
          'tabs-menu-content': {
            templateUrl: 'app/chat/chat-list/chat-list.html',
            controller: 'ChatListController as vm'
          }
        }
      })
      .state('menu.chat-detail', {
        url: '/chat-detail/:target',
        views: {
          'menu-content': {
            templateUrl: 'app/chat/chat-detail/chat-detail.html',
            controller: 'ChatDetailController as vm'
          }
        }
      });

    $urlRouterProvider.otherwise('/tabs/mates');
  }
})();
