(function() {
  'use strict';

  angular
    .module('app.database')
    .service('database', database);

  /* @ngInject */
  function database($q, $cordovaSQLite, DB_CONFIG) {
    var self = this;
    self.create = create;
    self.exec_sql = exec_sql;
    self.fetch_all = fetch_all;
    self.fetch = fetch;
    self.instance = null;

    function get_db() {
      if (self.instance == null) {
        self.instance = window.cordova ? $cordovaSQLite.openDB(DB_CONFIG.name, 1) : window.openDatabase(DB_CONFIG.name, "1.0.0", "YouMate DB", 1);
      }
      return self.instance;
    }
    
    function create() {
      DB_CONFIG.tables.forEach(function(table) {
        var sql = 'CREATE TABLE IF NOT EXISTS ' + table.name + '(' + table.columns + ')';
        exec_sql(sql);
      });
    }    

    function exec_sql(sql, binding) {
      var db = get_db();
      var deferred = $q.defer();

      function succ(tx, res) {
        console.log("ok: dbfactory", tx, res);
        deferred.resolve(res);
      }

      function error(tx, error) {
        deferred.reject(error);
        console.log("error: dbfactory", error);
      }

      db.transaction(function(tx) {
        tx.executeSql(sql, binding, succ, error);
      });

      return deferred.promise;
    }

    function fetch_all(result) {
      var output = [];

      for (var i = 0; i < result.rows.length; i++) {
        output.push(result.rows.item(i));
      }

      return output;
    }

    function fetch(result) {
      return result.rows.item(0);
    }
  }
})();
