(function() {
  'use strict';

  angular
    .module('app.database')
    .service('ChatsDao', ChatsDao);

  /* @ngInject */
  function ChatsDao($q, database, AuthUserService) {
    var self = this;
    self.add = add;
    self.addNotification = addNotification;
    self.get_channel_messages = get_channel_messages;
    self.get_last_chats = get_last_chats;
    self.remove_chat = remove_chat;
    self.countAllMsgNotRead = countAllMsgNotRead;

    self.MESSAGE_READ = 0;
    self.MESSAGE_NOT_READ = 1;

    function addChat(sql, chat, read) {
      var user = AuthUserService.getUser();
      var binding = [chat.id, chat.sent_by, chat.sent_to, chat.message, chat.channel, chat.timestamp, read, user.id];
      return database.exec_sql(sql, binding);
    }

    function add(chat) {
      var sql = 'INSERT OR REPLACE INTO chats (id, sent_by, sent_to, message, channel, timestamp, not_read, _user) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
      return addChat(sql, chat, self.MESSAGE_READ);
    }

    function addNotification(chat) {
      var sql = 'INSERT OR IGNORE INTO chats (id, sent_by, sent_to, message, channel, timestamp, not_read, _user) VALUES (?, ?, ?, ?, ?, ?, ?, ?)';
      addChat(sql, chat, self.MESSAGE_NOT_READ);
    }

    function get_channel_messages(channel) {
      var user = AuthUserService.getUser();
      var sql = 'SELECT *FROM chats WHERE channel=? and _user=? ORDER BY timestamp ASC';
      return database.exec_sql(sql, [channel, user.id]).then(function(res) {
        return database.fetch_all(res);
      });
    }

    function get_last_chats() {
      var user = AuthUserService.getUser();
      var sql = 'SELECT id, sent_by, sent_to, message, channel, timestamp, sum(not_read) as count \
                    FROM chats WHERE _user=? GROUP BY channel ORDER BY timestamp DESC';
      return database.exec_sql(sql, [user.id]).then(function(res) {
        return database.fetch_all(res);
      });
    }

    function remove_chat(chat) {
      var user = AuthUserService.getUser();
      var sql = 'DELETE FROM chats WHERE channel=? AND _user=?';
      return database.exec_sql(sql, [chat.channel, user.id]);
    }

    function countAllMsgNotRead() {
      var user = AuthUserService.getUser();
      var promise = $q.defer();
      if (user != null) {
        var sql = 'SELECT SUM(not_read) as count FROM chats WHERE _user=?';
        database.exec_sql(sql, [user.id]).then(function(res) {
          console.log('res', res);
          promise.resolve(database.fetch(res).count);
        }, function(error) {
          promise.resolve(0);
        });
      } else {
        promise.resolve(0);
      }
      return promise.promise;
    }
  }
})();
