(function() {
  'use strict';

  angular
    .module('app.database')
    .service('SearchMatesDao', SearchMatesDao);  

  /* @ngInject */
  function SearchMatesDao(database, AuthUserService) {
    var self = this;
    self.all = all;
    self.add = add;
    self.remove = remove;

    function all() {
      var user = AuthUserService.getUser();
      var sql = 'SELECT *FROM search_mates WHERE _user=? ORDER BY timestamp DESC';
      return database.exec_sql(sql, [user.id]).then(function(res) {
        return database.fetch_all(res);
      });
    }

    function add(search) {
      var user = AuthUserService.getUser();
      var sql = 'INSERT OR REPLACE INTO search_mates (search, _user) VALUES (?, ?)';
      return database.exec_sql(sql, [search, user.id]);
    }

    function remove(search_id) {
      var user = AuthUserService.getUser();
      var sql = 'DELETE FROM search_mates WHERE id=? AND _user=?';
      return database.exec_sql(sql, [search_id, user.id]);
    }
  }
})();
