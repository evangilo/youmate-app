(function() {
  'use strict';

  angular
    .module('app.database')
    .service('MateNotificationsDao', MateNotificationsDao);

  /* @ngInject */
  function MateNotificationsDao($q, database, AuthUserService) {
    var self = this;
    self.add = add;
    self.deleteAll = deleteAll;
    self.count = count;

    function add() {
      var user = AuthUserService.getUser();
      if (user != null) {
        var sql = 'INSERT INTO mate_notifications (user) values (?)';
        database.exec_sql(sql, [user.id]);
      }
    }

    function deleteAll() {
      var user = AuthUserService.getUser();
      var sql = 'DELETE FROM mate_notifications WHERE user=?';
      return database.exec_sql(sql, [user.id]);
    }

    function count() {
      var promise = $q.defer();
      var user = AuthUserService.getUser();
      var sql = 'SELECT COUNT(*) AS count FROM mate_notifications WHERE user=?';
      if (user != null) {
        database.exec_sql(sql, [user.id]).then(function(res) {
          promise.resolve(database.fetch(res).count);
        }, function(error) {
          promise.resolve(0);
        });
      } else {
        promise.resolve(0);
      }
      return promise.promise;
    }
  }
})();
