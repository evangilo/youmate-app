(function () {
  'use strict';

  angular
    .module('app.database')
    .constant('DB_CONFIG', {
      name: 'youmate.db',      
      tables: [{
          name: 'chats',
          columns: [
            'id TEXT PRIMARY KEY',
            'sent_by INTEGER NOT NULL',
            'sent_to INTEGER NOT NULL',
            'message TEXT NOT NULL',
            'channel TEXT NOT NULL',
            'not_read INTEGER DEFAULT 0',
            'timestamp REAL NOT NULL',
            '_user INTEGER NOT NULL'
          ]
        },
        {
          name: 'search_mates',
          columns: [
            'id INTEGER PRIMARY KEY AUTOINCREMENT',
            'search TEXT NOT NULL UNIQUE',
            'timestamp REAL DEFAULT CURRENT_TIMESTAMP',
            '_user INTEGER NOT NULL'
          ]
        },
        {
          name: 'mate_notifications',
          columns: [
            'id INTEGER PRIMARY KEY AUTOINCREMENT',
            'user INTEGER NOT NULL'
          ]
        }
      ]
    });
})();
