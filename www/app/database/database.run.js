(function() {
  'use strict';

  angular
    .module('app.database')
    .run(runDB)  

  /* @ngInject */
  function runDB($ionicPlatform, database) {
    $ionicPlatform.ready(database.create);
  }

})();
