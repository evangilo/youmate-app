(function () {
  'use strict';

  angular
    .module('app.api')
    .service('ApiService', ApiService);

  /* @ngInject */
  function ApiService($rootScope, $resource, AppConst, ApiPaginate) {
    var BASE_URL = AppConst.API_BASE_URL;
    var self = this;
    self.me = me;
    self.usersPage = usersPage;
    self.users = users;
    self.profiles = profiles;
    self.references = references;
    self.accessToken = accessToken;
    self.photos = photos;
    self.interests = interests;
    self.device = device;
    self.verifyPhone = verifyPhone;
    self.resendEmail = resendEmail;
    self.logout = logout;
    self.languages = languages;

    function transformProfile(profile, headers) {
      if (typeof (profile) === "string") {
        profile = angular.fromJson(profile);
      }
      profile.language_ids = [];
      profile.languages_formatted = [];
      profile.detail_insterests = [];

      for (var i in profile.languages) {
        var language = profile.languages[i];
        profile.language_ids.push(language.id);
        profile.languages_formatted.push(language.name);
      }
      for (var i in profile.interests) {
        var id = profile.interests[i];
        profile.detail_insterests.push(interests().get({id: id}));
      }
      profile.languages_formatted = profile.languages_formatted.join(", ");
      return profile;
    }

    function transformUser(userJson, headers) {
      var user = angular.fromJson(userJson);
      user.profile = transformProfile(user.profile);
      return user;
    }

    function usersPage() {
      return ApiPaginate.newInstance(BASE_URL + 'users/feed/');
    }

    function users() {
      return $resource(BASE_URL + 'users/:id/', {
        id: '@id'
      }, {
        get: {
          method: 'GET',
          transformResponse: transformUser
        },
        patch: {
          method: 'PATCH',
          transformResponse: transformUser
        }
      });
    }

    /**
     * description: returns the session user
     * @return {user}
     */
    function me() {
      return $resource(BASE_URL + 'users/logged_user/', {}, {
        get: {
          method: 'GET',
          transformResponse: transformUser
        },
        patch: {
          method: 'PATCH',
          transformResponse: transformUser
        }
      });
    }

    function photos() {
      return $resource(BASE_URL + 'photos/:id/', {
        id: '@id'
      });
    }

    function references() {
      return $resource(BASE_URL + 'references/:id/', {
        id: '@id'
      });
    }

    function interests() {
      return $resource(BASE_URL + 'interests/:id/', {
        id: '@id'
      }, {
        get: {
          method: 'GET',
          cache: true
        }
      });
    }

    function languages() {
      return $resource(BASE_URL + 'languages/:id/', {
        id: '@id'
      });
    }

    function verifyPhone() {
      return $resource(BASE_URL + 'confirmation/phone/:id/');
    }

    function profiles() {
      return $resource(BASE_URL + 'profiles/:id/:action/', {
        id: '@id'
      }, {
        get: {
          method: 'GET',
          transformResponse: transformProfile
        },
        patch: {
          method: 'PATCH',
          transformResponse: transformProfile
        },
        my_mates: {
          method: 'GET',
          params: {
            action: 'mates'
          }
        },

        pending_mates: {
          method: 'GET',
          params: {
            action: 'pending_mates'
          }
        },

        add_mate: {
          method: 'POST',
          params: {
            action: 'add_mate'
          }
        },

        accept_mate: {
          method: 'POST',
          params: {
            action: 'accept_mate'
          }
        },

        reject_mate: {
          method: 'POST',
          params: {
            action: 'reject_mate'
          }
        },

        cancel_mate: {
          method: 'DELETE',
          params: {
            action: 'cancel_mate'
          }
        },

        delete_mate: {
          method: 'DELETE',
          params: {
            action: 'delete_mate'
          }
        }
      });
    }

    function accessToken() {
      var credentials = {
        client_id: AppConst.YOUMATE_CLIENT_ID,
        client_secret: AppConst.YOUMATE_CLIENT_SECRET,
        grant_type: AppConst.YOUMATE_GRANT_TYPE,
        token: '@token',
        backend: '@backend'
      };

      return $resource(BASE_URL + 'rest-social-auth/convert-token/', {}, {
        get: {
          method: 'POST',
          params: credentials
        }
      });
    }

    function device() {
      return $resource(BASE_URL + 'device/:type/', {}, {
        post_gcm: {
          method: 'POST',
          params: {
            type: 'gcm'
          }
        },
        post_apns: {
          method: 'POST',
          params: {
            type: 'apns'
          }
        }
      });
    }

    function resendEmail() {
      return $resource(BASE_URL + 'profiles/reset_email_code/');
    }

    function logout() {
      return $resource(BASE_URL + 'profiles/logout/');
    }
  }
})();