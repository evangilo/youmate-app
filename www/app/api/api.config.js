(function() {
  'use strict';

  angular
    .module('app.api')
    .config(config);

  /* @ngInject */
  function config($resourceProvider, $httpProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
    $httpProvider.interceptors.push('ApiInterceptors')
    $httpProvider.defaults.headers['transformRequest'] = angular.identity;

    // Extra Methods
    $resourceProvider.defaults.actions.update = {method: 'PUT'};
    $resourceProvider.defaults.actions.patch = {method: 'PATCH'};    
  }
})();
