(function () {
  'use strict';

  angular
    .module('app.api')
    .service('PollsService', PollsService);

  /* @ngInject */
  function PollsService($resource, AppConst) {
    var BASE_URL = AppConst.API_BASE_URL + 'polls/';

    this.create = function (args) {
      return $resource(BASE_URL + 'create/')
        .save({
          text: args.text,
          interests: args.interests,
          address: args.address,
          latitude: args.latitude,
          longitude: args.longitude
        }).$promise;
    }

    this.update = function (id, text, interests, address, latitude, longitude) {
      return $resource(BASE_URL + ':id/update/', {
          id: '@id'
        })
        .patch({
          id: id,
          text: text,
          interests: interests,
          address: address,
          latitude: latitude,
          longitude: longitude
        });
    }

    /*
     * interests__id: Array<Number>
     * city: String
     * latitude: Number
     * longitude: Number
     */
    this.list = function (args) {
      if (!args.latitude && !args.longitude) {
        args.address = args.city;
        delete args.latitude;
        delete args.longitude;
      }
      delete args.city;
      return $resource(BASE_URL).query(args).$promise;
    }

    this.get = function (id) {
      return $resource(BASE_URL + ':id/', {
        id: '@id'
      }).get({
        id: id
      });
    }

    this.createAnwser = function (args) {
      return $resource(BASE_URL + ':pollId/answers/create/', {
          pollId: '@pollId'
        })
        .save({
          pollId: args.pollId,
          text: args.text
        }).$promise;
    }

    this.likeAction = function (pollId, answerId, liked) {
      var action = liked ? 'deslike/' : 'like/';
      var url = BASE_URL + pollId + '/answers/' + answerId + '/' + action;
      return $resource(url).save().$promise;
    }
  }
})();
