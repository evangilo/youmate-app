(function() {
  'use strict';

  angular
    .module('app.mates')
    .factory('ApiPaginate', ApiPaginate);

  /* @ngInject */
  function ApiPaginate($resource) {
  	var service = {
  		newInstance: newInstance
  	};

  	return service;

    function Paginate(url) {
      var self = this;
      self.page = {next: url};
      self.list = [];
      self.next = next;
      self._hasNext = true;
      self.hasNext = hasNext;
      self.isEmpty = isEmpty;
      self.reset = reset;

      function next(params, success, error) {
        var pageUrl = self.page.next;
        if (pageUrl) {
          $resource(pageUrl).get(params, function(page) {
            self.page = page;
            self._hasNext = page.next != null;

            page.results.forEach(function (data) {
              if (!_.find(self.list, {id: data.id})) {
                self.list.push(data);
              }
            });
            success(page.results, self);
          }, function(err) {
            error(err);
          });
        }
      }

      function hasNext() {
        return self.page.next != null;
      }

      function isEmpty() {
        return self.page.count != null && self.page.count == 0;
      }

      function reset() {
        self.page = {next: url};
        self._hasNext = true;
        self.list = [];
      }
    }

    function newInstance(url) {
  		return new Paginate(url);
  	}
  }
})();
