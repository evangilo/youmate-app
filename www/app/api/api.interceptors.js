(function() {
  'use strict';

  angular
    .module('app.api')
    .factory('ApiInterceptors', ApiInterceptors);

  /* @ngInject */
  function ApiInterceptors($q, $location, AuthUserService, AppConst) {
    var service = {
      request: request,
      responseError: responseError
    };
    return service;

    function request(config) {
      if (config.url.indexOf(AppConst.API_BASE_URL) == 0) {
        // config.headers['HTTP_APP_SECRET_KEY'] = AppConst.YOUMATE_APP_SECRET_KEY;
        var token = AuthUserService.getToken();
        if (token != null) {
          config.headers.Authorization = token.token_type + ' ' + token.access_token;
        }
      }
      return config;
    }

    function responseError(error) {
      if (error.status == 401) {
        $location.path('/menu/login');
      }
      return $q.reject(error);
    }
  }
})();
