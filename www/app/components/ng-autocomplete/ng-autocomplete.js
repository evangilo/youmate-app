(function() {
  'use strict';

  angular.module('ngAutocomplete', [])
    .directive('ngAutocomplete', ngAutocomplete);

  /* @ngInject */
  function ngAutocomplete($timeout) {
    var directive = {
      require: 'ngModel',
      link: link,
      scope: {
        ngModel: '=',
        geometry: '=',
        location: '='
      }
    };
    return directive;

    function link(scope, elem, attrs) {

      elem.bind('focus', function(event) {

        var autocomplete = new google.maps.places.Autocomplete(elem[0], {
          types: ['geocode']
        });

        autocomplete.addListener('place_changed', function() {
          var place = autocomplete.getPlace();

          console.log('found place', place.formatted_address, place);

          scope.$apply(function() {
            scope.ngModel = place.formatted_address;
            scope.geometry = {
              latitude: place.geometry.location.lat(),
              longitude: place.geometry.location.lng()
            }
            scope.location = {
              city: place.formatted_address,
              geometry: {
                latitude: place.geometry.location.lat(),
                longitude: place.geometry.location.lng()
              }
            };
            console.log('ngAutocomplete', scope.ngModel, scope.geometry);
          });
        });

        $timeout(function() {
          var container = document.querySelector('.pac-container');
          angular.element(container).attr('data-tap-disabled', 'true');
          angular.element(container).on("click", function(){
            elem[0].blur();
          });
        }, 500);
      });

      elem.bind('blur', function(event) {
        document.querySelector('.pac-container').remove();
      });
    }
  }
})();
