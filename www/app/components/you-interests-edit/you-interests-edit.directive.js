(function() {
  'use strict';

  angular
    .module('components.you-interests-edit')
    .directive('youInterestsEdit', youInterestsEdit);

  /* @ngInject */
  function youInterestsEdit() {
    var directive = {
      bindToController: true,
      controller: YouInterestsEditController,
      controllerAs: 'interestEdit',
      restrict: 'EA',
      replace: true,
      scope: {
        isLoaded: '=',
        checkList: '=',
      },
      templateUrl: 'app/components/you-interests-edit/you-interests-edit.html'
    };
    return directive;
  }

  /* @ngInject */
  function YouInterestsEditController($rootScope, $scope, $cordovaToast, ApiService) {
    var vm = this;
    vm.check = check;
    vm.interests = [];
    vm.isLoaded = false;

    initialize();

    function initialize() {
      vm.checkList = vm.checkList || [];
      ApiService.interests().query(function(interests) {
        vm.isLoaded = true;
        vm.interests = interests;

        vm.interests.forEach(function(interest) {
          if (vm.checkList.indexOf(interest.id) != -1) {
            interest.check = true;
          }
        });

      });
    }

    function check(interest) {
      if (!interest.check && vm.checkList.length == 4) {
        $cordovaToast.showShortCenter('Limite máximo de 4 interesses');
        return;
      }
      if (interest.check) {
        var index = vm.checkList.indexOf(interest.id);
        vm.checkList.splice(index, 1);
      } else {
        vm.checkList.push(interest.id);
      }
      interest.check = !interest.check;
    }

    $rootScope.$on('resetInterestsSelected', function() {
      console.log('resetInterestsSelected');
      angular.forEach(vm.interests, function (interest) {
        interest.check = false;
      });
    });
  }
})();
