(function() {
  'use strict';

  angular
    .module('components.you-scroll-to')
    .directive('youScrollTo', youScrollTo);

  /* @ngInject */
  function youScrollTo($ionicScrollDelegate, $location) {
    var directive = {
      link: link,
      restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs) {      
      element.bind('click', function() {
        $location.hash(attrs.hash);
        $ionicScrollDelegate.anchorScroll(true);
      });
    }
  }
})();
