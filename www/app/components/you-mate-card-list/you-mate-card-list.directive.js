(function() {
  'use strict';

  angular
    .module('components.you-mate-card-list')
    .directive('youMateCardList', youMateCardList);

  /* @ngInject */
  function youMateCardList() {
    var directive = {
      bindToController: true,
      controller: YouMateCardListController,
      controllerAs: 'vm',
      restrict: 'AE',
      scope: {
        users: '='
      },
      templateUrl: 'app/components/you-mate-card-list/you-mate-card-list.html'
    };
    return directive;
  }

  /* @ngInject */
  function YouMateCardListController() {
    var vm = this;
  }
})();
