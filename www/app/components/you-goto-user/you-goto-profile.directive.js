(function() {
  'use strict';

  angular
    .module('components.you-goto-profile')
    .directive('youGotoProfile', youGotoProfile);

  /* @ngInject */
  function youGotoProfile($state) {
    var directive = {
      link: link,
      restrict: 'A',
    };
    return directive;

    function link(scope, element, attrs) {
      element.bind('click', function() {                
        $state.go('menu.profile', {'id': attrs.userId});
      });
    }
  }
})();
