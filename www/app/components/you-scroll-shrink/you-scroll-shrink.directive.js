(function() {
  'use strict';

  angular
    .module('components.you-scroll-shrink')
    .directive('youScrollShrink', youScrollShrink);

  /* @ngInject */
  function youScrollShrink() {
    var directive = {
      link: link,
      restrict: 'A'
    };
    return directive;

    function link(scope, element, attr) {
      console.log('attr', attr);
      var y = 0;
      var prevY = 0;
      var delay = 0.9;
      var raw = element[0];
      var header = document.getElementById(attr.youScrollShrink);
      var offsetHeight = header.offsetHeight + 30;
      var fadeAmt;

      function onScroll() {
        y = (raw.scrollTop >= 0) ? Math.min(offsetHeight / delay, Math.max(0, y + raw.scrollTop - prevY)) : 0;
        fadeAmt = 1 - (y / offsetHeight);
        header.style[ionic.CSS.TRANSFORM] = 'translate3d(0, ' + -y + 'px, 0)';
        prevY = raw.scrollTop;
      }

      element.bind('scroll', onScroll);
    }
  }
})();
