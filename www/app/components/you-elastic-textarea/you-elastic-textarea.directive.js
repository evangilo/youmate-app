(function() {
  'use strict';

  angular
    .module('components.you-elastic-textarea')
    .directive('youElasticTextarea', youElasticTextarea);

  /* @ngInject */
  function youElasticTextarea() {
    var directive = {
      link: link,
      restrict: 'A',
      require: 'ngModel',
      scope: {
        ngModel: '='
      }
    };
    return directive;

    function link(scope, element, attrs, ngModel) {
      var elem = element[0];
      var maxRows = attrs.maxRows || 4;
      var maxScrollHeight = elem.scrollHeight * maxRows;      

      scope.$watch(function() {
        return ngModel.$modelValue;
      }, function(newValue) {
        if (maxScrollHeight > elem.scrollHeight) {
          elem.style.overflowY = 'scroll';
          elem.style.height = 'auto';
          elem.style.height = elem.scrollHeight + 'px';
        }
      });
    }
  }
})();
