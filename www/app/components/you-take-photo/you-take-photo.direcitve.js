(function () {
  'use strict';

  angular
    .module('components.you-take-photo')
    .directive('youTakePhoto', youTakePhoto);

  /* @ngInject */
  function youTakePhoto(Alerts, $cordovaCamera) {
    var directive = {
      restrict: 'AE',
      replace: false,
      link: link,
      scope: {
        takeSuccess: '='
      }
    };

    return directive;

    function link(scope, elem, attrs) {
      function getPhoto(isCamera) {
        var sourceType = isCamera ? Camera.PictureSourceType.CAMARA : Camera.PictureSourceType.PHOTOLIBRARY;
        var options = {
          quality: 100,
          destinationType: Camera.DestinationType.DATA_URL,
          sourceType: sourceType,
          targetWidth: 600,
          targetHeight: 600,
          allowEdit: true,
          encodingType: Camera.EncodingType.PNG,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false,
          correctOrientation: true
        };

        $cordovaCamera
          .getPicture(options)
          .then(function (image) {
            scope.takeSuccess("data:image/jpeg;base64," + image);
          }, function (error) {
            console.log(error);
          });
      }

      function take() {
        var options = {
          title: 'Tire uma Foto',
          message: 'Tire uma foto nova ou escolha uma da sua  galeria de fotos.',
          okText: 'Câmera',
          cancelText: 'Galeria',
          okCallback: function () {
            getPhoto(true);
          },
          cancelCallback: function () {
            getPhoto(false);
          }
        }
        Alerts.showConfirm(options);
      }

      elem.bind('click', take);
    }
  }
})();