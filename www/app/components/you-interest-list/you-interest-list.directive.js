(function () {
  'use strict';

  angular
    .module('components.you-interest-list')
    .directive('youInterestList', youInterestList);

  /**
   * attrs: ngModel com uma lista de ID de interesses.
   */
  /* @ngInject */
  function youInterestList($ionicModal) {
    var directive = {
      bindToController: true,
      controller: YouInterestListController,
      controllerAs: 'vm',
      restrict: 'AE',
      scope: {
        'interests': '=',
        'showModal': '@',
      },
      template: '<img class="icon" ng-src="{{ interest.url || interest.picture }}" ng-repeat="interest in vm.interests" ng-click="vm.showInterestsModal()">'
    };
    return directive;
  }

  /* @ngInject */
  function YouInterestListController($scope, $rootScope, $ionicModal, ApiService) {
    var vm = this;
    vm.showInterestsModal = showInterestsModal;
    vm.closeInterestsModal = closeInterestsModal;

    function showInterestsModal() {
      if (vm.showModal == 'true' || vm.showModal == undefined) {
        $scope.modal.show();
      }
    }

    function closeInterestsModal() {
      $scope.modal.hide();
    }

    $ionicModal.fromTemplateUrl('app/components/you-interest-list/you-interest-list.modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
    });
  }
})();