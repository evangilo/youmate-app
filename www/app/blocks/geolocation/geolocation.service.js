(function() {
  'use strict';

  angular
    .module('blocks.geolocation')
    .factory('geolocation', geolocation);

  /* @ngInject */
  function geolocation($http, $cordovaGeolocation) {
    var service = {
      location: getLocation,
      distance: distance,
      distanceBetweenCurrentLocation: distanceBetweenCurrentLocation
    };
    return service;

    function getLocation(callback) {
      var options = { timeout: 5000, enableHighAccuracy: false, maximumAge: 60000 };

      $cordovaGeolocation
        .getCurrentPosition(options)
        .then(function(position) {

          var res = {
            city: '',
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          };

          var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + res.latitude + ',' + res.longitude;

          $http.get(url)
            .then(function(data, headers) {
              res.city = format_address(data.data);
              callback(res);
            }, function(error) {
              callback(res);
            });

        }, function(err) {
          callback({});
        });
    }

    function distance(origin, destiny, callback) {
      var service = new google.maps.DistanceMatrixService();
      var options = {
        origins: [origin],
        destinations: [destiny],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      };

      service.getDistanceMatrix(options, function(response, status) {
        callback(response.rows[0].elements[0].distance.value);
      }, function(error) {
        callback(0);
      });
    }

    function distanceBetweenCurrentLocation(destiny, callback) {
      getLocation(function(place) {
        distance(place.city, destiny, callback);
      });
    }

    function format_address(address) {
      var places = address.results;
      for (var key in places) {
        if (places[key].geometry.location_type == 'APPROXIMATE') {
          return places[key].formatted_address;
        }
      }
      return '';
    }
  }
})();
