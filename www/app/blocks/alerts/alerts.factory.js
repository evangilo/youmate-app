(function() {
  'use strict';

  angular
    .module('blocks.alerts')
    .factory('Alerts', Alerts);

  /* @ngInject */
  function Alerts($q, $ionicLoading, $ionicPopup, $cordovaDialogs, $cordovaSpinnerDialog, $cordovaToast) {
    return window.cordova ? NativeAlerts() : BrowserAlerts();

    function NativeAlerts() {

      function showProgressDialog(title, message) {
        $cordovaSpinnerDialog.show(title, message, true);
      }

      function hideProgressDialog() {
        $cordovaSpinnerDialog.hide();
      }

      function showCenterToast(message) {
        $cordovaToast.showLongCenter(message);
      }

      function showBottomToast(message) {
        $cordovaToast.showLongBottom(message);
      }

      /**
       * confirm dialog
       * @param  {Object} params {message, title, okText, cancelText, okCallback, cancelCallback}       
       */
      function showConfirm(params) {
        var okText = params['okText'] || '';
        var cancelText = params['cancelText'] || '';
        var title = params['title'] || '';
        var message = params['message'] || '';
        $cordovaDialogs.confirm(message, title, [okText, cancelText])
          .then(function(buttonIndex) {
            var okCallback = params['okCallback'];
            var cancelCallback = params['cancelCallback'];
            if (buttonIndex == 1 && okCallback) {
              okCallback();
            } else if (buttonIndex == 2 && cancelCallback) {
              cancelCallback();
            }
          });
      }

      return {
        showProgressDialog: showProgressDialog,
        hideProgressDialog: hideProgressDialog,
        showCenterToast: showCenterToast,
        showBottomToast: showBottomToast,
        showConfirm: showConfirm
      };
    }

    function BrowserAlerts() {
      function showProgressDialog(title, message) {
        $ionicLoading.show({
          template: message,
          title: title
        });
      }

      function hideProgressDialog() {
        $ionicLoading.hide();
      }

      function showCenterToast(message) {
        $ionicPopup.show({
          template: message
        });
      }

      function showBottomToast(message) {
        $ionicPopup.show({
          template: message
        });
      }

      function showConfirm(params) {
        var confirmPopup = $ionicPopup.confirm({
          title: params['title'],
          template: params['message'],
          okText: params['okText'] || '',
          cancelText: params['cancelText'] || ''
        });

        confirmPopup.then(function(res) {
          var okCallback = params['okCallback'];
          var cancelCallback = params['cancelCallback'];
          if (res && okCallback) {
            okCallback();
          } else if (cancelCallback) {
            cancelCallback();
          }
        });
      }

      return {
        showProgressDialog: showProgressDialog,
        hideProgressDialog: hideProgressDialog,
        showCenterToast: showCenterToast,
        showBottomToast: showBottomToast,
        showConfirm: showConfirm
      };
    }
  }
})();
