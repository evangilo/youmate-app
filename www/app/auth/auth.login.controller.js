(function() {
  'use strict';

  angular
    .module('app.auth')
    .controller('LoginController', LoginController);

  /* @ngInject */
  function LoginController($scope, $location, $ionicHistory) {
    var vm = this;
    vm.goHome = goHome;

    $scope.$on('$ionicView.beforeEnter', function(event, viewData) {
      viewData.enableBack = true;
    });

    function goHome() {
      $location.path('/tabs/mates');
      $location.replace();
      $ionicHistory.clearHistory();
    }
  }
})();
