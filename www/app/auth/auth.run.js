(function() {
  'use strict';

  angular
    .module('app.auth')
    .run(run);

  /* @ngInject */
  function run($rootScope, $location, $ionicHistory, AuthUserService) {

    function redirectView(url) {
      $ionicHistory.currentView($ionicHistory.backView());
      $location.path(url);
    }

    function isForbiddenUrl(url) {
      var urls = ['', '/menu/login', '/tabs/mates'];
      return urls.indexOf(url) === -1;
    }

    $rootScope.$on('$locationChangeStart', function(event, next, current) {
      var user = AuthUserService.getUser();

      if (isForbiddenUrl($location.url()) && user === null) {
        redirectView('/menu/login');
      } else if (user !== null && user.profile.interests.length === 0) {
        redirectView('/menu/after-login');
      }
    });
  }

})();
