(function() {
  'use strict';

  angular
    .module('app.auth')
    .directive('youButtonGoogleLogin', youButtonGoogleLogin);

  /* @ngInject */
  function youButtonGoogleLogin() {
    var directive = {
      controller: ButtonGoogleLoginController,
      controllerAs: 'googleButton',
      restrict: 'E',
      template: '<button class="button icon-left ion-social-google btn-google" ng-click="googleButton.login()">Google</button>'
    };
    return directive;
  }

  /* @ngInject */
  function ButtonGoogleLoginController(AuthService) {
    var vm = this;
    vm.login = login;

    function login() {
      AuthService.loginGoogle();
    }
  }
})();
