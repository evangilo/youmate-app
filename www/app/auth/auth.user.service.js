(function() {
  'use strict';

  angular
    .module('app.auth')
    .service('AuthUserService', AuthUserService);

  /* @ngInject */
  function AuthUserService() {
    var self = this;
    self.getUser = getUser;
    self.setUser = setUser;
    self.setProfile = setProfile;
    self.getToken = getToken;
    self.setToken = setToken;
    self.clearData = clearData;
    self.isLogged = isLogged;

    function getUser() {
      try {
        return JSON.parse(localStorage.user);
      } catch (error) {
        return null;
      }
    }

    function setUser(user) {
      localStorage.user = JSON.stringify(user);
    }

    function setProfile(profile) {
      try {
        var user =  getUser();
        user.profile = profile;
        setUser(user);
      } catch(error) {

      }
    }

    function getToken() {
      try {
        return JSON.parse(localStorage.token);
      } catch (error) {
        return null;
      }
    }

    function setToken(token) {
      localStorage.token = JSON.stringify(token);
    }

    function clearData() {
      delete localStorage.user;
      delete localStorage.token;
    }

    function isLogged() {
      return getUser() != null;
    }
  }
})();
