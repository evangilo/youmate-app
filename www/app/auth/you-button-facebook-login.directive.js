(function() {
  'use strict';

  angular
    .module('app.auth')
    .directive('youButtonFacebookLogin', youButtonFacebookLogin);

  /* @ngInject */
  function youButtonFacebookLogin() {
    var directive = {
      controller: ButtonFacebookLoginController,
      controllerAs: 'facebookButton',
      restrict: 'E',
      template: '<button class="button icon-left ion-social-facebook btn-facebook" ng-click="facebookButton.login()">Facebook</button>'
    };
    return directive;
  }

  /* @ngInject */
  function ButtonFacebookLoginController(AuthService) {
    var vm = this;
    vm.login = login;

    function login() {
      console.log("facebook");
      AuthService.loginFacebook();
    }
  }
})();
