(function() {
  'use strict';

  angular
    .module('app.auth')
    .service('AuthService', AuthService);

  /* @ngInject */
  function AuthService($rootScope, $location, $ionicHistory,
    $window, $cordovaSpinnerDialog, $cordovaToast, $timeout,
    pubnub, AppConst, ApiService, AuthUserService, ProfileService) {
    var self = this;
    self.loginFacebook = loginFacebook;
    self.loginGoogle = loginGoogle;
    self.logout = logout;

    function logout() {
      var isAndroid = ionic.Platform.isAndroid();
      var device_id = isAndroid ? parseInt(window.device.uuid, 16) : window.device.uuid;

      pubnub.unregisterNotification();

      if (localStorage.backend === 'google') {
        window.plugins.googleplus.disconnect(function (msg) {
          console.log('logout google', msg);
        });
      } else {
        facebookConnectPlugin.logout(function(sucess) {
          console.log('logout facebook', sucess);
        }, function(error) {
          console.error('logout facebook', error);
        });
      }
      AuthUserService.clearData();
      $rootScope.pubnub = null;
      $ionicHistory.clearCache().then(function() {
        $location.path('/tabs/mates');
      });
    }

    function loginGoogle() {
      showLoading();
      var options = { 'offline': true, 'webApiKey': AppConst.GOOGLE_WEB_API_KEY };
      window.plugins.googleplus.login(options, function(res) {
        localStorage.backend = 'google';
        loginYouMate(res.oauthToken, AppConst.GOOGLE_BACKEND);
      }, function(error) {
        if (error === 'user cancelled') {
          hideLoading();
        } else {
          errorLogin();
        }
      });
    }

    function loginFacebook() {
      showLoading();
      facebookConnectPlugin.login(['public_profile', 'email', 'user_birthday'], function(res) {
        console.log(res);
        localStorage.backend = 'facebook';
        loginYouMate(res.authResponse.accessToken, AppConst.FACEBOOK_BACKEND);
      }, function(error) {
        if (error.errorCode === '4201') {
          hideLoading();
        } else {
          errorLogin();
        }
      });
    }

    function loginYouMate(token, backend) {
      var params = { token: token, backend: backend };
      ApiService.accessToken().get(params, function(accessToken) {
        AuthUserService.setToken(accessToken);
        ProfileService.updateStatusLocation();
        getAndSaveMe();
      }, function(error) {
        errorLogin();
      })
    }

    function getAndSaveMe() {
      ApiService.me().get(function(user) {
        AuthUserService.setUser(user);
        pubnub.init();
        pubnub.registerNotification();
        postDeviceId(user);
        successLogin(user);
      }, function(error) {
        errorLogin();
      })
    }

    function postDeviceId(user) {
      var isAndroid = ionic.Platform.isAndroid();
      var isIOS = ionic.Platform.isIOS();
      var device = {
        user: user.id,
        registration_id: $rootScope.deviceRegId,
        device_id: window.device.uuid
      };

      if (isAndroid) {
        device.device_id = parseInt(device.device_id, 16);
        ApiService.device().post_gcm(device);
      } else if (isIOS) {
        ApiService.device().post_apns(device);
      }
    }

    function successLogin(user) {
      hideLoading();
      showMessage("Usuário logado!");
      if (user.profile.interests.length) {
        $location.path('/tabs/mate-list');
      } else {
        $location.path('/menu/after-login');
      }
      pubnub.init();
      $rootScope.$emit('successLogin');
    }

    function errorLogin() {
      showMessage("Ocorreu um erro. Tente novamente mais tarde!");
      hideLoading();
    }

    function showLoading() {
      $cordovaSpinnerDialog.show("", "Logando...", true);
    }

    function hideLoading() {
      $cordovaSpinnerDialog.hide();
    }

    function showMessage(message) {
      $cordovaToast.showLongCenter(message);
    }
  }
})();
