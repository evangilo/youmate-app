(function() {
  'use strict';

  angular
    .module('app.auth')
    .directive('youLogout', youLogout);

  /* @ngInject */
  function youLogout(AuthService, Alerts) {
    var directive = {
      link: link,
      restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs) {
      element.bind('click', function() {
        Alerts.showConfirm({
          title: "Sair",
          message: "Deseja sair?",
          okText: "Sim",
          cancelText: "Não",
          okCallback: function() {
            AuthService.logout();
          }
        })
      });
    }
  }
})();
