(function() {
  'use strict';

  angular
    .module('app.menus')
    .controller('TabsController', TabsController);

  /* @ngInject */
  function TabsController($rootScope, $scope, ChatsDao, MateNotificationsDao) {
    var vm = this;
    vm.countMessages = 0;
    vm.mateNotifications = 0;

    $scope.$on('$ionicView.beforeEnter', function() {
      updateCountMessages();
      updateCountMateNofications();
    });

    function updateCountMessages() {
      ChatsDao.countAllMsgNotRead().then(function(count) {
        vm.countMessages = count > 99 ? 99 : count;
      });
    }

    function updateCountMateNofications() {
      MateNotificationsDao.count().then(function(count) {
        vm.mateNotifications = count > 99 ? 99 : count;
      })
    }

    $rootScope.$on('newMessages', updateCountMessages);

    $rootScope.$on('changeNotificationOfMates', updateCountMateNofications);
  }
})();
