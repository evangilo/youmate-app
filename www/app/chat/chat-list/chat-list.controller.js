(function() {
  'use strict';

  angular
    .module('app.chat')
    .controller('ChatListController', ChatListController);


  /* @ngInject */
  function ChatListController($rootScope, $scope, $state, ApiService, ChatsDao, AuthUserService, Alerts) {
    /* jshint validthis: true */
    var vm = this;
    vm.user = null;
    vm.users = {};
    vm.loadChats = loadChats;
    vm.onItemClick = onItemClick;
    vm.removeChat = removeChat;

    $scope.$on('$ionicView.loaded', function() {
      vm.user = AuthUserService.getUser();
    });

    $scope.$on('$ionicView.beforeEnter', loadChats);

    $rootScope.$on('newMessages', loadChats);

    function loadChats() {
      ChatsDao.get_last_chats().then(function(chats) {
        console.log(chats);
        vm.chats = chats;
        vm.chats.forEach(function(chat) {
          chat.user = getUser(chat.sent_by, chat.sent_to);
        });
      });
    }

    function getUser(sent_by, sent_to) {
      var id = vm.user.id == sent_by ? sent_to : sent_by;
      var user = vm.users[id] ? getUserLocalStorage(id) : getUserRemoteStorage(id);
      return user;
    }

    function getUserLocalStorage(id) {
      return vm.users[id];
    }

    function getUserRemoteStorage(id) {
      return ApiService.users().get({ id: id }, function(user) {
        vm.users[id] = user;
      });
    }

    function onItemClick(user) {
      $state.go('menu.chat-detail', { 'target': JSON.stringify(user) }, { location: 'replace' });
    }

    function removeChat(index) {
      Alerts.showConfirm({
        message: 'Excluir a conversa?',
        okText: 'Sim',
        cancelText: 'Não',
        okCallback: function() {
          ChatsDao.remove_chat(vm.chats[index]);
          vm.chats.splice(index, 1);
        }
      })
      console.log(index);
    }
  }

})();
