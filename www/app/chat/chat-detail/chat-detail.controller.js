(function() {
  'use strict';

  angular
    .module('app.chat')
    .controller('ChatDetailController', ChatDetailController);

  /* @ngInject */
  function ChatDetailController($scope, $stateParams, $ionicScrollDelegate,
                                $timeout, $ionicPlatform, $ionicHistory, 
                                $cordovaActionSheet, $cordovaClipboard,
                                ChatDetailService, ApiService, AuthUserService,
                                Alerts) {
    /* jshint validthis: true */
    var vm = this;
    vm.copyText = copyText;
    vm.goBack = goBack;
    vm.inputDown = inputDown;
    vm.inputUp = inputUp;
    vm.sendMessage = sendMessage;
    vm.showActionSheet = showActionSheet;

    $scope.$on('$ionicView.beforeEnter', initialize);

    function initialize() {
      vm.user = AuthUserService.getUser();
      vm.target = JSON.parse($stateParams.target);
      if (typeof(vm.target) == "number") {
        ApiService.users().get({ id: vm.target }, function(user) {
          vm.target = user;
          initChatService();
        });
      } else {
        initChatService();
      }
    }

    function initChatService() {
      ChatDetailService.connect(vm.user, vm.target);
      vm.messages = ChatDetailService.messages;
    }

    $ionicPlatform.onHardwareBackButton(function(event) {
      _setOnffline();
    });

    function copyText(text) {      
      $cordovaClipboard.copy(text).then(function() {
        Alerts.showCenterToast('Mensagem copiada.');
      });
    }

    function goBack() {
      $ionicHistory.goBack();
      _setOnffline();
    }

    function _setOnffline() {
      ChatDetailService.disconnect();
    }

    function inputUp() {
      this.keyboardHeight = 216;
      $timeout(function() {
        $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(true);
      }, 300);
    }

    function inputDown() {
      this.keyboardHeight = 0;
      $ionicScrollDelegate.$getByHandle('chatScroll').resize();
    }

    function sendMessage() {
      ChatDetailService.sendMessage(vm.newMessage);
      vm.newMessage = null;
    }

    function showActionSheet(message) {
      var options = {
        title: 'Falha ao enviar a mensagem',
        buttonLabels: ['Reenviar Messagem', 'Deletar Mensagem']        
      };
      $cordovaActionSheet.show(options).then(function(btnIndex) {
        if (btnIndex == 1) {
          ChatDetailService.resendMessage(message);
        } else if(btnIndex == 2) {
          ChatDetailService.deleteMessage(message);
        }
      })
    }

    $scope.$watchCollection('vm.messages', function(newVal, oldVal) {
      var animate = false;
      if (oldVal && newVal) {
        animate = oldVal.length !== newVal.length;
      }
      $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(animate);
    });
  }

})();
