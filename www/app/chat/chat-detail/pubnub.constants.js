(function() {
  'use strict';

  angular
    .module('app.chat')
    .constant('ChatConst', {
      'PUB_KEY': 'pub-c-6142eab7-65c9-4234-a9fb-29714b4b4c40',
      'SUB_KEY': 'sub-c-f051c666-ddd0-11e5-8905-02ee2ddab7fe',
      'BASE_CHANNEL': 'youmate:'
    });
})();
