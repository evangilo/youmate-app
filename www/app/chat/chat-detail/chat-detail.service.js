(function() {
  'use strict';

  angular
    .module('app.chat')
    .service('ChatDetailService', ChatDetailService);

  /* @ngInject */
  function ChatDetailService($rootScope, $filter, pubnub, uuid, ChatsDao, ChatConst) {
    var self = this;
    self.connect = connect;
    self.disconnect = disconnect;
    self.sendMessage = sendMessage;
    self.resendMessage = resendMessage;
    self.deleteMessage = deleteMessage;

    self.SENT_MESSAGE_SUCCESS = 'sent';
    self.SENT_MESSAGE_FAILED = 'error';

    function connect(user, target) {
      self.user = user;
      self.target = target;
      self.isTargetOnline = false;
      self.messages = {};
      self.channel = ChatConst.BASE_CHANNEL + [user.id, target.id].sort().join(":");
      self.pubnub = pubnub.init();
      _retrieveLocalMessages();
      _setupSubscribe();
      _setupHistory();
    }

    function disconnect() {
      self.pubnub.unsubscribe({ channel: self.channel });
    }

    function deleteMessage(message) {
      delete self.messages[message.id];
    }

    function sendMessage(text) {
      self.pubnub.time(function(timetoken) {
        var message = {
          id: uuid.get(),
          message: text,
          sent_by: self.user.id,
          sent_to: self.target.id,
          channel: self.channel,
          timestamp: new Date(+timetoken / 10000)
        };
        _handlerMessage(message);
      });
    }

    function resendMessage(message) {
      message.timestamp = Date.now();
      delete message.status;
      _handlerMessage(message);
    }

    function _handlerMessage(message) {
      if (!self.isTargetOnline) {
        _sendNotification(message);
      }
      _addMessage(message);
      _sendMessage(message);
    }

    function _retrieveLocalMessages() {
      ChatsDao.get_channel_messages(self.channel).then(function(messages) {
        messages.forEach(function(message) {
          message.timestamp = new Date(message.timestamp);
          _addMessage(message);
        })
      });
    }

    function _addMessage(message, saveInTheLocalDB) {
      if (!self.messages[message.id]) {
        _safeApply(function() {
          message.day = _formateDate(message.timestamp);
          self.messages[message.id] = message;
        });
      }

      if (saveInTheLocalDB) {
        ChatsDao.add(message);
      }
    }

    function _safeApply(fn) {
      ($rootScope.$$phase || $rootScope.$$phase) ? fn(): $rootScope.$apply(fn);
    }

    function _setState(state) {
      self.pubnub.state({
        channel: self.channel,
        uuid: self.user.id,
        state: state,
        callback: _onPresence
      });
    }

    function _sendMessage(message) {
      console.log("sent message");
      self.pubnub.publish({
        channel: self.channel,
        message: message,
        callback: function(message) { _sentMessageSuccess(message); },
        error: function(error) { _sentMessageFailed(message); }
      });
    }

    function _sendNotification(message) {
      console.log("sent notification");
      message['title'] = 'YouMate: ' + self.user.first_name + ' ' + self.user.last_name;
      message['image'] = self.user.profile.photo_url;
      message['notId'] = self.user.id;
      message['style'] = 'inbox';
      message['summaryText'] = '%n% novas mensagens';
      message['content-available'] = 1;
      message['type'] = 'message';
      pubnub.sendNotificationTo(self.target, message);
    }

    function _sentMessageSuccess(message) {
      _safeApply(function() {
        message.status = self.SENT_MESSAGE_SUCCESS;
      });
      ChatsDao.add(message);
    }

    function _sentMessageFailed(message) {
      _safeApply(function() {
        message.status = self.SENT_MESSAGE_FAILED;
      });
    }

    function _formateDate(date) {
      return $filter('date')(date, 'dd/MM/yyyy');
    }

    function _onReceiveMessage(message, envelope, channelOrGroup, time, channel) {
      if (Array.isArray(message)) {
        message[0].forEach(function(message) {
          _addMessage(message, true);
        });
      } else {
        _addMessage(message, true);
      }
      console.log('messages after', self.messages);
      _.orderBy(self.messages, ['timestamp'], ['asc']);
      console.log('messages before', self.messages);
    }

    function _onPresence(info) {
      console.log(info);
      self.isTargetOnline = info.occupancy == 2;
    }

    function _setupSubscribe() {
      self.pubnub.subscribe({
        channel: self.channel,
        message: _onReceiveMessage,
        presence: _onPresence
      });
    }

    function _setupHistory() {
      self.pubnub.history({
        channel: self.channel,
        callback: _onReceiveMessage,
        count: 500
      });
    }
  }
})();
