(function() {
  'use strict';

  angular
    .module('app.chat')
    .service('pubnub', pubnub);

  /* @ngInject */
  function pubnub($rootScope, AuthUserService, ChatConst) {
    var self = this;
    self.init = init;
    self.sendNotificationTo = sendNotificationTo;
    self.unregisterNotification = unregisterNotification;
    self.registerNotification = registerNotification;

    $rootScope.$on('successLogin', init);

    function getPubnub() {
      return init() || {};
    }

    function init() {
      console.log('inicializando pubnub');
      var user = AuthUserService.getUser();
      if (user && !$rootScope.pubnub) {
        _setupPubnub(user);
        _subscribeRootChannel(user);
        _setupRootNotification(user);
      }
      return $rootScope.pubnub;
    }

    function sendNotificationTo(user, notification) {
      if (user != null || notification != null) {
        getPubnub().publish({
          channel: _getRootChannel(user),
          store_in_history: false,
          message: { pn_gcm: { data: notification } }
        });
      }
    }

    function _getRootChannel(user) {
      return ChatConst.BASE_CHANNEL + ':root:' + user.id;
    }

    function _setupPubnub(user) {
      $rootScope.pubnub = PUBNUB.init({
        publish_key: ChatConst.PUB_KEY,
        subscribe_key: ChatConst.SUB_KEY,
        uuid: user.id,
        callback: function(info) { console.log('ok:init pubnub', info) },
        error: function(error) { console.log('error:init pubnub:', error); }
      });
    }

    function _subscribeRootChannel(user) {
      getPubnub().subscribe({
        channel: _getRootChannel(user)
      });
    }

    function unregisterNotification() {
      var user = AuthUserService.getUser();
      getPubnub().mobile_gw_provision({
        device_id: $rootScope.deviceRegId,
        op: 'remove',
        gw_type: 'gcm', // or 'gcm'
        channel: _getRootChannel(user),
        callback: function(info) { console.log('ok: setup notification', info) },
        error: function(error) { console.log('error: setup notification', error) },
      });
    }

    function registerNotification() {
      _setupRootNotification(AuthUserService.getUser());
    }

    function _setupRootNotification(user) {
      getPubnub().mobile_gw_provision({
        device_id: $rootScope.deviceRegId,
        op: 'add',
        gw_type: 'gcm', // or 'gcm'
        channel: _getRootChannel(user),
        callback: function(info) { console.log('ok: setup notification', info) },
        error: function(error) { console.log('error: setup notification', error) },
      });
    }
  }
})();
