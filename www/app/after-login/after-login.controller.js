(function() {
  'use strict';

  angular
    .module('app.after-login')
    .controller('AfterLoginController', AfterLoginController);

  /* @ngInject */
  function AfterLoginController($scope, $location, $ionicHistory, ApiService, AuthUserService) {
    var vm = this;
    vm.user = null;
    vm.params = {};
    vm.save = save

    $scope.$on('$ionicView.afterEnter', initialize);

    function initialize() {
      vm.params.genre = 'X';
      vm.params.living_city = '';
      vm.params.interests = [];
    }

    function save() {
      var user = AuthUserService.getUser();
      ApiService.profiles().patch({id: user.profile.id}, vm.params, function(profile) {
        user.profile = profile;
        AuthUserService.setUser(user);
        $location.path('/tabs/mates');
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
      });
    }
  }
})();
