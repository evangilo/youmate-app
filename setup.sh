sudo apt-get install curl
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
npm install
bower install
ionic platform add android
cp config_files/debug.keystore platforms/android/
cp config_files/debug-signing.properties platforms/android/
cp config_files/google-services.json platforms/android/
unzip -o -d platforms/android/ config_files/icones-push-notification/ic_stat_youmate.zip
echo "\nDone."
